var app = {};
var listUl = document.querySelector('#todolist');
var inputItem = document.querySelector('#todo_item');
var addItem = document.querySelector('#add_todo');

// intiialize the application
function initApp() {
    showTodos(getTodos());

    listUl.addEventListener('click', toggleItem);
    addItem.addEventListener('click', addTodo);
};

// show all todo items
function showTodos(todoList) {
    var todoItem, listEl, listElText;

    if (todoList) {
        listUl.innerHTML = '';

        for (key in todoList) {
            todoItem = todoList[key];
            listEl = document.createElement('li');
            listEl.classList.add(todoItem.status);
            listEl.setAttribute('data-id', key);

            listElText = document.createTextNode(todoItem.title);
            listEl.appendChild(listElText);

            listUl.appendChild(listEl);
        }
    }
};

// save todo list to some storage
function saveTodos(todoList) {
    // {"1":{"title":"fsdfd","status":"complete"},"2":{"title":"sljfdlk","status":"open"}}
    localStorage.todo = JSON.stringify(todoList);
};

// read todos from some storage
function getTodos() {
    var todos = [];
    var todoLS = localStorage.getItem('todo'); // {"1":{"title":"fsdfd","status":"complete"},"2":{"title":"sljfdlk","status":"open"}}
    var todoObj = {};

    if (todoLS) {
        var todoObj = JSON.parse(todoLS);
    }

    app.todoList = todoObj;
    return todoObj;
};

// add a new item
function addTodo() {
    var newItem = inputItem.value;

    if(newItem) {
        var key = Object.keys(app.todoList).length;

        app.todoList[++key] = {
            'title':  newItem,
            'status': 'open'
        };

        inputItem.value = '';
    }

    showTodos(app.todoList);
    saveTodos(app.todoList);
};

// mark/unmark a todo item
function toggleItem(event) {
    if(event.target.tagName == 'LI') {
        var listItem = event.target;
        var listItemId;

        if(listItem.classList[0] == 'open') {
            listItem.classList.remove('open');
            listItem.classList.add('complete');

            listItemId = listItem.dataset.id;
            app.todoList[listItemId].status = 'complete';
        } else {
            listItem.classList.remove('complete');
            listItem.classList.add('open');

            listItemId = listItem.dataset.id;
            app.todoList[listItemId].status = 'open';
        }
    }

    saveTodos(app.todoList);
}

// ready event
document.addEventListener('DOMContentLoaded', initApp, false);
