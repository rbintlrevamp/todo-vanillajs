var http = require("http");
var path = require("path");
var fs   = require("fs");
var PORT = 8080;

http.createServer(function (request, response) {

    var filePath =  request.url;
    filePath = "./public/"+filePath;

    if (request.url == '/')
        filePath = filePath +'index.html';


    
    var extname = path.extname(filePath);
    var contentType = 'text/html';
    switch (extname) {
        case '.js':
            contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
    }

    fs.readFile(filePath, function(error, content) {
        if (error) {
            console.log(error)
        }
        else {
            response.writeHead(200, { 'Content-Type': contentType });
            response.end(content, 'utf-8');
        }
    });

}).listen(PORT);
console.log('Server running at http://127.0.0.1:'+PORT);